# Cloud run

Create the infrastructure on GCP needed to build and deploy the
[merge-request-bot](https://gitlab.com/merge-request-bot/merge-request-bot)
on [Google Cloud Run](https://cloud.google.com/run/)

## Configure

### Service Account

To run pulumi on GCP you would need a [service
account](https://cloud.google.com/compute/docs/access/service-accounts)
with the correct permissions to create the resources, below is the list
of roles that are needed:

- Role Administrator
- Service Account Admin
- Project IAM Admin
- Cloud Run Admin
- Cloud Run Service Agent
- Storage Admin

![gcp roles example](https://i.imgur.com/R4CpxLk.png)

After the service account is created set the
`GOOGLE_APPLICATION_CREDENTIALS`
[variable](https://docs.gitlab.com/ee/ci/variables/) which is [file
based](https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables-of-type-file)
with the JSON value of the service account.

![gitlab variables](https://i.imgur.com/rseOSys.png)

### Pulumi Access Token

Follow the official [Pulumi
documentation](https://www.pulumi.com/docs/guides/continuous-delivery/gitlab-ci/#environment-variables)
to set up the correct environment variables to run Pulumi.

### Variables for deployment

You have the specify the following [CI/CD
variables](https://docs.gitlab.com/ee/ci/variables/) so that Cloud Run
deployment is configured properly.

- `GITLAB_GRAPHQL`: The GraphQL endpoint of the GitLab instance for
  example `https://gitlab.com/api/graphql`
- `GITLAB_PAT`: The [personal access
  token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
  that is going to be used to post the comment. The `API` scope is
  required.
- `GITLAB_WEBHOOK_SECRET_TOKEN`: A secret token to be used to [webhook
  secret](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#secret-token).
  You should generate a random string.

![gitlab variable example for deployment](https://i.imgur.com/Y6n564t.png)
