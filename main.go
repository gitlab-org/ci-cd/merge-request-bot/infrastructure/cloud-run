package main

import (
	"os"

	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/cloudrun"
	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/projects"
	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/serviceAccount"
	"github.com/pulumi/pulumi-gcp/sdk/v3/go/gcp/storage"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
)

const srvName = "merge-request-bot"
const region = "europe-west4"
const srvImg = "gcr.io/group-verify-df9383/merge-request-bot"

const (
	gitlabGraphQL      = "GITLAB_GRAPHQL"
	gitlabPAT          = "GITLAB_PAT"
	gitlabWebhookToken = "GITLAB_WEBHOOK_SECRET_TOKEN"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		// Create a GCP resource (Storage Bucket) for cloud build logs.
		bucket, err := storage.NewBucket(ctx, "merge-request-bot-cloud-build", nil)
		if err != nil {
			return err
		}

		ctx.Export("bucketName", bucket.Url)

		// Create Role for cloud run deployer.
		role, err := projects.NewIAMCustomRole(ctx, "merge-request-bot-deployer", &projects.IAMCustomRoleArgs{
			Description: pulumi.StringPtr("Role used to build with cloud build and deploy to cloud run."),
			Permissions: pulumi.StringArray{
				pulumi.String("storage.objects.list"),
				pulumi.String("storage.objects.get"),
				pulumi.String("storage.objects.create"),
				pulumi.String("storage.buckets.list"),
				pulumi.String("storage.buckets.create"),
				pulumi.String("storage.buckets.get"),
				pulumi.String("cloudbuild.builds.get"),
				pulumi.String("cloudbuild.builds.list"),
				pulumi.String("cloudbuild.builds.create"),
				pulumi.String("iam.serviceAccounts.actAs"),
				pulumi.String("run.services.update"),
				pulumi.String("run.services.get"),
			},
			RoleId: pulumi.String("merge_request_bot_deployer"),
			Stage:  pulumi.StringPtr("ALPHA"),
			Title:  pulumi.String("merge-request-bot-deployer"),
		})
		if err != nil {
			return err
		}

		ctx.Export("roleID", role.RoleId)

		// Create Service Account for cloudrun deployer.
		acc, err := serviceAccount.NewAccount(ctx, "merge-request-bot", &serviceAccount.AccountArgs{
			AccountId:   pulumi.String("merge-request-bot-deployer"),
			Description: pulumi.StringPtr("Service account used to build and deploy the merge request bot"),
			DisplayName: pulumi.StringPtr("merge-request-bot-deployer"),
		})
		if err != nil {
			return err
		}

		ctx.Export("accountEmail", acc.Email)

		// Attach role to the service account.
		binding, err := projects.NewIAMBinding(ctx, "merge-request-bot-deployer", &projects.IAMBindingArgs{
			Members: pulumi.StringArray{
				pulumi.Sprintf("serviceAccount:%s", acc.Email),
			},
			Role: role.ID(),
		})
		if err != nil {
			return err
		}

		ctx.Export("bindingMembers", binding.Members)

		// Create cloud run service.
		srv, err := cloudrun.NewService(ctx, "merge-request-bot", &cloudrun.ServiceArgs{
			Location: pulumi.String(region),
			Name:     pulumi.String(srvName),
			Template: cloudrun.ServiceTemplateArgs{
				Metadata: nil,
				Spec: cloudrun.ServiceTemplateSpecArgs{
					ContainerConcurrency: pulumi.IntPtr(80),
					Containers: cloudrun.ServiceTemplateSpecContainerArray{
						cloudrun.ServiceTemplateSpecContainerArgs{
							Image: pulumi.String(srvImg),
							Resources: cloudrun.ServiceTemplateSpecContainerResourcesArgs{
								Limits: pulumi.StringMap{
									"memory": pulumi.String("256Mi"),
									"cpu":    pulumi.String("1000m"),
								},
							},
							Envs: cloudrun.ServiceTemplateSpecContainerEnvArray{
								cloudrun.ServiceTemplateSpecContainerEnvArgs{
									Name:  pulumi.String(gitlabGraphQL),
									Value: pulumi.String(os.Getenv(gitlabGraphQL)),
								},
								cloudrun.ServiceTemplateSpecContainerEnvArgs{
									Name:  pulumi.String(gitlabPAT),
									Value: pulumi.String(os.Getenv(gitlabPAT)),
								},
								cloudrun.ServiceTemplateSpecContainerEnvArgs{
									Name:  pulumi.String(gitlabWebhookToken),
									Value: pulumi.String(os.Getenv(gitlabWebhookToken)),
								},
							},
						},
					},
				},
			},
		})
		if err != nil {
			return err
		}

		ctx.Export("serviceURL", srv.Status.Url())

		// Make cloudrun service accessible to all users.
		srvBinding, err := cloudrun.NewIamBinding(ctx, "public", &cloudrun.IamBindingArgs{
			Location: srv.Location,
			Members: pulumi.StringArray{
				pulumi.String("allUsers"),
			},
			Role:    pulumi.String("roles/run.invoker"),
			Service: srv.Name,
		})
		if err != nil {
			return err
		}

		ctx.Export("serviceMembers", srvBinding.Members)

		return nil
	})
}
